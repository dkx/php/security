<?php

declare(strict_types=1);

namespace DKX\SecurityTests;

use DKX\Security\Exception\ForbiddenException;
use DKX\Security\Identity\AuthenticatedIdentity;
use DKX\Security\Identity\GuestIdentity;
use DKX\Security\Identity\Identity;
use DKX\Security\Security;
use DKX\Security\Votes\SecurityAwareVoter;
use DKX\Security\Votes\Voter;
use PHPUnit\Framework\TestCase;

final class SecurityTest extends TestCase
{


	public function testAuthenticate(): void
	{
		$security = new Security;
		self::assertFalse($security->getIdentity()->isAuthenticated());
		self::assertEquals([], $security->getIdentity()->getRoles());

		$security->authenticate(new AuthenticatedIdentity(new class {}, ['ROLE_ADMIN']));
		self::assertTrue($security->getIdentity()->isAuthenticated());
		self::assertEquals(['ROLE_ADMIN'], $security->getIdentity()->getRoles());

		$security->logout();
		self::assertFalse($security->getIdentity()->isAuthenticated());
		self::assertEquals([], $security->getIdentity()->getRoles());
	}


	public function testIsIdentityGranted_false(): void
	{
		$security = new Security;
		self::assertFalse($security->isIdentityGranted(new GuestIdentity,'ROLE_ADMIN'));
	}


	public function testIsIdentityGranted_guest_true(): void
	{
		$security = new Security;
		self::assertTrue($security->isIdentityGranted(new GuestIdentity, Security::IS_GUEST));
	}


	public function testIsIdentityGranted_authenticated_true(): void
	{
		$security = new Security;
		$identity = new AuthenticatedIdentity(new class {});

		self::assertTrue($security->isIdentityGranted($identity, Security::IS_AUTHENTICATED));
	}


	public function testIsIdentityGranted_role_true(): void
	{
		$security = new Security;
		$identity = new AuthenticatedIdentity(new class {}, ['ROLE_ADMIN']);

		self::assertTrue($security->isIdentityGranted($identity, 'ROLE_ADMIN'));
	}


	public function testIsIdentityGranted_voter_true(): void
	{
		$voter = new class implements Voter
		{
			public function supports(string $attribute, object $subject): bool
			{
				return true;
			}

			public function voteOnAttribute(string $attribute, object $subject, Identity $identity): bool
			{
				return true;
			}
		};

		$security = new Security;
		$security->addVoter($voter);

		self::assertTrue($security->isIdentityGranted(new GuestIdentity, 'create', new class {}));
	}


	public function testDenyAccessUnlessGranted(): void
	{
		self::expectException(ForbiddenException::class);
		self::expectExceptionMessage('Access denied for "ROLE_ADMIN"');

		$security = new Security;
		$security->denyAccessUnlessGranted('ROLE_ADMIN');
	}


	public function testAddVoter_inject_security(): void
	{
		$voter = new class implements Voter, SecurityAwareVoter
		{
			/** @var \DKX\Security\Security */
			public $security;

			public function setSecurity(Security $security): void
			{
				$this->security = $security;
			}

			public function supports(string $attribute, object $subject): bool
			{
				return true;
			}

			public function voteOnAttribute(string $attribute, object $subject, Identity $identity): bool
			{
				return true;
			}
		};

		$security = new Security;
		$security->addVoter($voter);

		self::assertSame($security, $voter->security);
	}

}
