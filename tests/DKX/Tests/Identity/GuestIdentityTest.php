<?php

declare(strict_types=1);

namespace DKX\SecurityTests\Identity;

use DKX\Security\Identity\GuestIdentity;
use PHPUnit\Framework\TestCase;

final class GuestIdentityTest extends TestCase
{


	public function testIsAuthenticated(): void
	{
		$identity = new GuestIdentity;
		self::assertFalse($identity->isAuthenticated());
	}


	public function testGetRoles(): void
	{
		$identity = new GuestIdentity;
		self::assertEquals([], $identity->getRoles());
	}

}
