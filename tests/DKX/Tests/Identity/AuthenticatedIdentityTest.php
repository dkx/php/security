<?php

declare(strict_types=1);

namespace DKX\SecurityTests\Identity;

use DKX\Security\Identity\AuthenticatedIdentity;
use PHPUnit\Framework\TestCase;

final class AuthenticatedIdentityTest extends TestCase
{


	public function testGetUser(): void
	{
		$user = new class {};
		$identity = new AuthenticatedIdentity($user);
		self::assertSame($user, $identity->getUser());
	}


	public function testIsAuthenticated(): void
	{
		$identity = new AuthenticatedIdentity(new class {});
		self::assertTrue($identity->isAuthenticated());
	}


	public function testGetRoles(): void
	{
		$identity = new AuthenticatedIdentity(new class {}, ['ROLE_ADMIN']);
		self::assertEquals(['ROLE_ADMIN'], $identity->getRoles());
	}

}
