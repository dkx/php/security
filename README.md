# DKX/Security

Security package for PHP based on symfony security and voters for stateless apps.

## Installation

```bash
$ composer require dkx/security
```

## Usage

```php
<?php

use DKX\Security\Security;
use DKX\Security\Identity\AuthenticatedIdentity;

$security = new Security;
$identity = new AuthenticatedIdentity($user, ['ROLE_ADMIN']);

$security->authenticate($identity);

var_dump($security->getIdentity());
```

## Authentication

Simple authentication can be seen in the example above.

Calling `getIdentity()` will always return some identity (interface `Identity`). It will be `GuestIdentity` for 
unauthenticated user.

You could easily create custom identity class. Only requirement is that it must implement the `Identity` interface.

## Check privileges

```php
<?php

use DKX\Security\Security;
use DKX\Security\Identity\AuthenticatedIdentity;

$security = new Security;

$security->isGranted(Security::IS_GUEST);          // true
$security->isGranted(Security::IS_AUTHENTICATED);  // false
$security->isGranted('ROLE_ADMIN');                // false

$security->authenticate(new AuthenticatedIdentity($user, ['ROLE_ADMIN']));

$security->isGranted(Security::IS_GUEST);          // false
$security->isGranted(Security::IS_AUTHENTICATED);  // true
$security->isGranted('ROLE_ADMIN');                // true

$security->logout();

$security->isGranted(Security::IS_GUEST);          // true
$security->isGranted(Security::IS_AUTHENTICATED);  // false
$security->isGranted('ROLE_ADMIN');                // false
```

## Voters

Voters can be used for advanced privileges checks. They allow to eg. check if specific user has access to specific 
resource.

```php
<?php

use DKX\Security\Security;
use DKX\Security\Votes\Voter;
use DKX\Security\Identity\Identity;
use DKX\Security\Identity\GuestIdentity;

class BookVoter implements Voter
{
    public const CREATE = 'create';
    
    public function supports(string $attribute, object $subject): bool
    {
        if (!\in_array($attribute, [self::CREATE], true)) {
            return false;
        }
        
        if (!$subject instanceof Book) {
            return false;
        }
        
        return true;
    }

    public function voteOnAttribute(string $attribute, object $subject, Identity $identity): bool
    {
        if ($identity instanceof GuestIdentity) {
            return false;
        }
        
        switch ($attribute) {
            case self::CREATE: return $this->canCreate($subject, $identity);
        }
        
        // should be unreachable
        return false;
    }
    
    private function canCreate(Book $book, Identity $identity): bool 
    {
        return true;
    }
}

$security = new Security;
$security->addVoter(new BookVoter);

$security->isGranted(BookVoter::CREATE, $book);
```

If you need to access `Security` inside of voter, implement the `SecurityAwareVoter` interface.
