<?php

declare(strict_types=1);

namespace DKX\Security\Votes;

use DKX\Security\Security;

interface SecurityAwareVoter
{


	public function setSecurity(Security $security): void;

}
