<?php

declare(strict_types=1);

namespace DKX\Security\Votes;

use DKX\Security\Identity\Identity;

interface Voter
{


	public function supports(string $attribute, object $subject): bool;


	public function voteOnAttribute(string $attribute, object $subject, Identity $identity): bool;

}
