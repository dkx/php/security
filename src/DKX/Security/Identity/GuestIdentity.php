<?php

declare(strict_types=1);

namespace DKX\Security\Identity;

final class GuestIdentity implements Identity
{


	public function isAuthenticated(): bool
	{
		return false;
	}


	public function getRoles(): array
	{
		return [];
	}

}
