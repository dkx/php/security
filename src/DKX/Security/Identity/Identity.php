<?php

declare(strict_types=1);

namespace DKX\Security\Identity;

interface Identity
{


	public function isAuthenticated(): bool;


	/**
	 * @return string[]
	 */
	public function getRoles(): array;

}
