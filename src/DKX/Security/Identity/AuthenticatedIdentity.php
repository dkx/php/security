<?php

declare(strict_types=1);

namespace DKX\Security\Identity;

final class AuthenticatedIdentity implements Identity
{


	/** @var object */
	private $user;

	/** @var string[] */
	private $roles;


	/**
	 * @param object $user
	 * @param string[] $roles
	 */
	public function __construct(object $user, array $roles = [])
	{
		$this->user = $user;
		$this->roles = $roles;
	}


	public function getUser(): object
	{
		return $this->user;
	}


	public function isAuthenticated(): bool
	{
		return true;
	}


	/**
	 * @return string[]
	 */
	public function getRoles(): array
	{
		return $this->roles;
	}

}
