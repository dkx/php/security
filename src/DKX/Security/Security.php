<?php

declare(strict_types=1);

namespace DKX\Security;

use DKX\Security\Exception\ForbiddenException;
use DKX\Security\Identity\GuestIdentity;
use DKX\Security\Identity\Identity;
use DKX\Security\Votes\SecurityAwareVoter;
use DKX\Security\Votes\Voter;

final class Security
{


	public const IS_GUEST = 'IS_GUEST';

	public const IS_AUTHENTICATED = 'IS_AUTHENTICATED';


	/** @var \DKX\Security\Identity\Identity */
	private $identity;

	/** @var \DKX\Security\Votes\Voter[] */
	private $voters = [];


	public function __construct()
	{
		$this->identity = new GuestIdentity;
	}


	public function addVoter(Voter $voter): void
	{
		$this->voters[] = $voter;

		if ($voter instanceof SecurityAwareVoter) {
			$voter->setSecurity($this);
		}
	}


	public function authenticate(Identity $identity): void
	{
		$this->identity = $identity;
	}


	public function logout(): void
	{
		$this->identity = new GuestIdentity;
	}


	public function getIdentity(): Identity
	{
		return $this->identity;
	}


	public function isIdentityGranted(Identity $identity, string $attribute, ?object $subject = null): bool
	{
		$isAuthenticated = $identity->isAuthenticated();

		if ($attribute === self::IS_GUEST && !$isAuthenticated) {
			return true;
		}

		if ($attribute === self::IS_AUTHENTICATED && $isAuthenticated) {
			return true;
		}

		if ($subject === null && \in_array($attribute, $identity->getRoles(), true)) {
			return true;
		}

		if ($subject !== null) {
			$voters = $this->getActiveVoters($attribute, $subject);
			foreach ($voters as $voter) {
				if ($voter->voteOnAttribute($attribute, $subject, $identity)) {
					return true;
				}
			}
		}

		return false;
	}


	public function isGranted(string $attribute, ?object $subject = null): bool
	{
		return $this->isIdentityGranted($this->identity, $attribute, $subject);
	}


	public function denyAccessUnlessGranted(string $attribute, ?object $subject = null): void
	{
		if (!$this->isGranted($attribute, $subject)) {
			throw new ForbiddenException('Access denied for "'. $attribute. '"');
		}
	}


	/**
	 * @param string $attribute
	 * @param object $subject
	 * @return \DKX\Security\Votes\Voter[]
	 */
	private function getActiveVoters(string $attribute, object $subject): array
	{
		return \array_filter($this->voters, function (Voter $voter) use ($attribute, $subject) {
			return $voter->supports($attribute, $subject);
		});
	}

}
