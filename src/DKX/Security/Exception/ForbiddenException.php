<?php

declare(strict_types=1);

namespace DKX\Security\Exception;

final class ForbiddenException extends \RuntimeException
{

}
